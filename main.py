import calendar
from datetime import datetime

print('Календарь\n')

# Get current year and month
now = datetime.now()
year = now.year
month = now.month

print(calendar.month(year, month))